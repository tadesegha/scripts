& $psscriptroot/install-chocolatey.ps1

if (get-command git -erroraction silentlycontinue) { return }

cinst git
& $psscriptroot/refresh-environment-variables.ps1
