function download-config {
  if (test-path $home/appdata/local/nvim) { return }
  git clone git@bitbucket.org:tadesegha/nvim $home/appdata/local/nvim
}

function download-minpac {
  if (test-path $home/appdata/local/nvim/pack/minpac) { return }
  git clone https://github.com/k-takata/minpac.git $home/appdata/local/nvim/pack/minpac/opt/minpac
  nvim -c "call minpac#update('', { 'do': 'quit' })"
}

function install-neovim {
  cinst neovim
  $env:path += ';c:\tools\neovim\neovim\bin'
}

& $psscriptroot/install-chocolatey
& $psscriptroot/install-git.ps1
install-neovim
download-config
download-minpac
