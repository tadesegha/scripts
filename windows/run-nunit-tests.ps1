param ($fullyQualifiedNameOfTest)

$csproj = dir -recurse -filter *Test*.csproj
$csprojDirectory = (split-path $csproj.FullName)

set-location $csprojDirectory
msbuild /v:quiet

if ($lastexitcode -ne 0) {
  exit
}

$testAssemblyName = $csproj -replace 'csproj', 'dll'
$testAssembly = get-item bin/debug/**/$testAssemblyName

if ($fullyQualifiedNameOfTest) {
  write-host "nunit3-console.exe $testAssembly --test $fullyQualifiedNameOfTest"
  nunit3-console.exe $testAssembly --test $fullyQualifiedNameOfTest
} else {
  write-host "nunit3-console.exe $testAssembly"
  nunit3-console.exe $testAssembly
}
