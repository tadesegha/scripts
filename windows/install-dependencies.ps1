param ([Parameter(Mandatory = $true)][array] $dependencies)

foreach ($dependency in $dependencies) {
	if (-not $dependency.isInstalled) { invoke-command ($dependency.install) }
}
