$chocolatey = @'

#Chocolatey profile
$ChocolateyProfile = "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
if (Test-Path($ChocolateyProfile)) {
  Import-Module "$ChocolateyProfile"
}
'@

function create-profile {
  if (test-path $profile) { throw 'profile already exists' }

  new-item -type file -path $profile -force
  set-content -path $profile -value ('$env:path += ";{0}"' -f $psscriptroot)
  add-content -path $profile -value $chocolatey
  add-content -path $profile -value '[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"'
}

$dependencies = @(
  @{ isInstalled = test-path $profile; install = { create-profile }}
)

& $psscriptroot/install-dependencies.ps1 $dependencies
