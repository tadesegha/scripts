$locations = 'HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\Environment',
             'HKCU:\Environment'

foreach ($location in $locations) {
  $registryKey = get-item $location

  foreach ($name in $registryKey.GetValueNames()) {
    $value = $registryKey.GetValue($name)

    if ($userLocation -and $name -ieq 'PATH') {
      $env:path += ";$value"
    } else {
      set-item -path env:\$name -value $value
    }
  }

  $userLocation = $true
}
