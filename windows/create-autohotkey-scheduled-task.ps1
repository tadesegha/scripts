$userName = [System.Security.Principal.WindowsIdentity]::GetCurrent().Name

$action = New-ScheduledTaskAction -Execute 'powershell' -argument "-windowstyle hidden -command ""$psscriptroot/autohotkey.ahk"""
$trigger = New-ScheduledTaskTrigger -AtLogon
$user = New-ScheduledTaskPrincipal $userName -runlevel Highest

$settings = New-ScheduledTaskSettingsSet
$settings.StopIfGoingOnBatteries = $false
$settings.DisallowStartIfOnBatteries = $false

$task = New-ScheduledTask -Action $action -Principal $user -Trigger $trigger -Settings $settings
Register-ScheduledTask "start.autohotkey" -InputObject $task
