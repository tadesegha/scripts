#! /usr/bin/env bash

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew cask install google-chrome karabiner-elements iterm2
brew install neovim fzf ag python3

ssh-keygen
echo 'set up ssh access to bitbucket'
echo 'push enter to continue'
read

git config --global user.name 'tolu adesegha'
git config --global user.email tolu@adesegha.ca

git clone git@bitbucket.org:tadesegha/karabiner.git ~/.config/karabiner
git clone git@bitbucket.org:tadesegha/nvim.git ~/.config/nvim
git clone https://github.com/k-takata/minpac.git ~/.config/nvim/pack/minpac/opt/minpac
git clone https://github.com/omnisharp/omnisharp-roslyn.git ~/.omnisharp/omnisharp-roslyn

echo "
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad

alias ls='ls -GFh'" >> ~/.bash_profile

echo "set bell-style none
set editing-mode vi" >> ~/.inputrc

echo 'remember to update minpac'
